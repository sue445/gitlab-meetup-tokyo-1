# ここがすごいよGitLab CI #gitlabjp
sue445

2017/03/02 [GitLab Meetup Tokyo #1](https://gitlab-jp.connpass.com/event/49755/)

---
## 自己紹介 [![sue445](images/sue445.png)](https://twitter.com/sue445)
* Go Sueyoshi a.k.a [sue445](https://twitter.com/sue445)
* [株式会社ドリコム](http://www.drecom.co.jp/) 所属
  * インフラストラクチャー部
  * 社内gemを中心にアプリからインフラまでだいたいサーバサイドを浅く広く見てる
* 今まで作った主要なものは [sue445 Advent Calendar 2016 - Qiita](http://qiita.com/advent-calendar/2016/sue445) を参照
* 「ドリコムのプリキュアの人」として社内外で有名
* Twitterは上級者向けのアカウントなのでフォロー厳禁 :warning:

---
### GitLab関係で作ったものをいくつかピックアップ
* [GitLab Notifier for Google Chrome™](http://qiita.com/sue445/items/630003a5d617a4ea17bc)
  * GitLabの通知を受け取るためのChrome拡張
* [Jenkins GitLab Logo Plugin](http://qiita.com/sue445/items/4d14d56e26cd1db37166)
  * JenkinsにGitLabのプロジェクトのロゴを表示
* [gitlab_awesome_release](https://gitlab.com/sue445/gitlab_awesome_release)
  * gitのtagとMRからいい感じにCHANGELOGを生成
* [gitlab_mr_release](http://qiita.com/sue445/items/a168e57d70712e3f30e7)
  * develop -> masterみたいなリリースMRを作る

---
### 今期の嫁：キュアカスタード
![cure custard](images/cure_custard.jpg)

---
### 本気の嫁：キュアピース
![cure peace](images/cure_engineer_peace.jpg)

---
## :beer: スポンサートーク :beer:

---
## ![drecom](images/drecom_1.png)

---
## ![drecom](images/drecom_2.png)

---
## ![drecom](images/drecom_logo.png)
* http://www.drecom.co.jp/
* 目黒
  * ゲーム：Ruby on Rails, Unity, Cocos2d-x
  * 広告：Elixir, Phoenix
* 社員数：280人（うちエンジニアは2〜3割くらい）
* 技術ブログ：https://tech.drecom.co.jp/

---
## アジェンダ
* ドリコムとGitLab
* GitLab CIについて
* ドリコムでの利用例

---
## ドリコムとGitLab
* 2012年12月頃導入
* 1800リポジトリ、49グループ、gitリポジトリ450GB
  * バックアップはEBS snapshotでいい感じにやってる
* 詳しいことは [ドリコムの開発を支えるGitリポジトリ](http://gussan.hateblo.jp/entry/2014/12/05/%E3%83%89%E3%83%AA%E3%82%B3%E3%83%A0%E3%81%AE%E9%96%8B%E7%99%BA%E3%82%92%E6%94%AF%E3%81%88%E3%82%8BGit%E3%83%AA%E3%83%9D%E3%82%B8%E3%83%88%E3%83%AA) 参照

![drecom-gitlab](images/drecom-gitlab.png)

---
## GitLab CIについて
* GitLab付属のCIツール
* https://about.gitlab.com/gitlab-ci/
* 元々GitLab（リポジトリ）とGitLab CIは別アプリだったが、v8.0.0で本体に統合された
* リポジトリと一体化してるのでJenkinsと比べて設定や連携がすごい楽

---
### 構成
![architecture](images/arch-1.jpg)

* GitLabのアプリサーバ1台に対して、Runner（Jenkinsのslaveのようなもの）を複数構成
  * https://docs.gitlab.com/runner/
  * https://gitlab.com/gitlab-org/gitlab-ci-multi-runner
* GitLabからRunnerにsshで接続したり、Runner内でDockerのイメージを起動してビルドを実行する（Runner起動時に選べる）

---
## ドリコムでの利用例
* Railsアプリ開発
* gem開発
* packageのビルド＆デプロイ
* Terraform実行

---
### Railsアプリ開発
![pipeline_rails](images/pipeline_rails.png)

* rspec, [index_shotgun](http://qiita.com/sue445/items/7a2f6eaafda172ef537f)（不要なインデックスを検知するgem）, rubocopを並列実行
* 全て問題なければdevelopブランチの時だけstagingにデプロイ
* 詳しくは [GitLab CIでRailsアプリをお手軽CI開発する - Tech Inside Drecom](https://tech.drecom.co.jp/easy-ci-development-using-gitlab-ci/) を参照

---
### yamlだけでスッキリ書ける
```yaml
# .gitlab-ci.yml（中略）
stages:
  - test
  - deploy
rspec:
  stage: test
  script: ./gitlab-ci/rspec.sh
index_shotgun:
  stage: test
  script: ./gitlab-ci/index_shotgun.sh
rubocop:
  stage: test
  script: ./gitlab-ci/rubocop.sh
  allow_failure: true
deploy_staging:
  stage: deploy
  script: ./gitlab-ci/deploy.sh staging
  only:
    - develop
```

---
#### Werckerでも似たようなことはできる
![wercker_workflow](images/wercker_workflow.png)

* ジョブの枝分かれはできるけど、枝分かれ後の合流ができない。
  * 2つ以上のジョブが全部終わったら次のジョブを実行するというのができない

---
#### 画面ポチる必要があってyamlだけで完結しないのが難点
![wercker_setting](images/wercker_setting.png)

---
### gem開発
![pipeline_gem](images/pipeline_gem.png)

* Travis CIのように複数のRubyのバージョンでテストを実行

---
#### GitLab CIだと複数のDockerfileを指定できる
```yaml
# .gitlab-ci.yml
.job_template: &job_definition
  # 共通する設定をここに書く
ruby2.1:rspec:
  <<: *job_definition
  image: drecom/centos-ruby:2.1.10
ruby2.2:rspec:
  <<: *job_definition
  image: drecom/centos-ruby:2.2.6
ruby2.3:rspec:
  <<: *job_definition
  image: drecom/centos-ruby:2.3.3
ruby2.4:rspec:
  <<: *job_definition
  image: drecom/centos-ruby:2.4.0
rubocop:
  <<: *job_definition
  image: drecom/centos-ruby
```

* WerckerもDockerイメージを使えるが1つしか使えない
* `.job_template` のようにドットで始まるkeyはGitLab CIで無視されるので、共通設定を抽出するのに使える
* 各Rubyのバージョンでrspecを実行
* rubocopは各Rubyのバージョンで動かす意味はあまりないので最新のRubyのみで実行
* OSSで公開してるgemで実際に使ってるファイル： https://gitlab.com/sue445/gitlab_awesome_release/blob/master/.gitlab-ci.yml

---
#### GitLab CIで向いていない事例
![jenkins_axis](images/jenkins_axis.png)

* ビルドパラメータの軸が2つ以上ある場合GitLab CIだと厳しいのでJenkinsやTravis CI使うのがいい
  * Jenkinsだと拙作の [Yaml Axis Plugin](https://wiki.jenkins-ci.org/display/JENKINS/Yaml+Axis+Plugin) 使うと便利

---
### packageのビルド＆デプロイ
![package_build](images/package_build.jpg)

* ①のパッケージ作成をGitLab CIで自動化してる
* 詳しくは弊社インフラエンジニアの [現代ITインフラの王道をゆくLinuxパッケージ管理の基本構成 \| 外道父の匠](http://blog.father.gedow.net/2016/03/08/package-control/) 参照

---
### GitLab CIでやってること
![pipeline_package](images/pipeline_package.png)

* ブランチをpushしたらpackageをビルド
* tagをpushしたらpackageをビルドし、rpmファイルやdebファイルをS3にpush
* あとはlambdaがいい感じにpackageを公開してくれる

---
#### .gitlab-ci.yml（一部）
```yaml
before_script:
  - docker info
stages:
  - build
  - deploy
build_centos5:
  stage: build
  script:
    - ./gitlab-ci/build.sh centos5
  except:
    - tags
deploy_centos5:
  stage: deploy
  script:
    - ./gitlab-ci/build.sh centos5 deploy
  only:
    - tags
```

---
### Terraform実行
* Terraformとはインフラの構成管理ツール
  * https://www.terraform.io/
* ドリコムではサービスごとにAWSのアカウントを管理していて、全てのアカウントを同じ構成にするためにTerraformを利用している
* 手で実行すると面倒なことが多いので（後述）、実行をGitLab CIで自動化した

---
#### 構成
![terraform](images/terraform.png)

* account1-terraform
  * AWSのアカウントごとに作ったリポジトリ（ドリコムだと10個以上ある）
  * 実行したいterraformのファイルを配置
* terraform-base
  * terraformを実際に実行するためのスクリプトを配置したリポジトリ
  * account1-terraformからcloneして使う
  * スクリプトの共通化のためにリポジトリを分割

---
#### tarraform plan（本実行前の変更内容の確認）の流れ
1. MRで `tarraform plan` を実行
  * terraform.tfstate（Terraformの状態管理ファイル）が暗号化されてコミットされてるので実行前に復号化する
  * terraform.tfstateにはRDSのパスワードやIAMのキーが生で入ってるので直接git管理したくない
  * ローカル実行だと手元のterraform.tfstateが古い場合に大惨事になるので、確実に最新のterraform.tfstateを使うためにGitLab CIで自動実行してる
2. ビルド時のログでplan結果を見て問題なければMRをアクセプト

---
#### terraform apply（本実行）の流れ
1. MRをアクセプトしたら `tarraform apply`が自動実行
  * 実行前に暗号化されてるterraform.tfstateを復号化する
2. `tarraform apply`が終わったらterraform.tfstateを暗号化してcommitしてpushする
  * コミットメッセージに [ci skip] を入れないと、さらに `terraform apply` してcommitしてpushしてという無限ループになるので注意

---
## 【まとめ】GitLab CIの所感
* GitLab CIはyamlだけでビルドの並列・直列実行が簡単にできて、今まで自分が使ったCI系のツール(Travis CI, Circle CI, Wercker, Jenkins)の中では一番自分の理想に一番近い
* GitLabの他にはビルドを実行するRunnerを用意すればいいので、既存リポジトリへの導入が楽
  * 1箇所設定できてしまえば後は .gitlab-ci.yml を他リポジトリにコピペするだけで使える

---
## 【おまけ】GitLab CIでGitLab Pagesにページを公開する
* GitLab PagesとはGitLab CIを利用してGitLab上で静的サイトを公開する仕組み（GitHub PagesのGitLab版）
* 元々Enterprise Editionのみの機能だったが、8.17でCommunity Editionでも使えるようになった :tada:
  * https://about.gitlab.com/2017/02/22/gitlab-8-17-released/
* 詳しいこと
  * https://docs.gitlab.com/ee/user/project/pages/index.html
  * https://about.gitlab.com/2016/04/07/gitlab-pages-setup/
* https://gitlab.com/ でもGitLab Pagesが使える

---
### このスライドもGitLab CIでビルドしてGitLab.comのGitLab Pagesに公開しています :innocent:

* https://sue445.gitlab.io/gitlab-meetup-tokyo-1/
* https://gitlab.com/sue445/gitlab-meetup-tokyo-1

---
## WE’RE HIRING !!
メンバー積極採用中です。
社員・契約社員・個人事業主・アルバイト etc..
http://www.drecom.co.jp/recruit/

### 募集職種
* エンジニア
  * Web/アプリ/フロントエンド
  * 基盤・インフラ
* デザイナー
* ディレクター/プランナー
* データサイエンティスト and more !!
