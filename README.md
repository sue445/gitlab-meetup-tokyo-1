# ここがすごいよGitLab CI #gitlabjp

[GitLab Meetup Tokyo #1](https://gitlab-jp.connpass.com/event/49755/) の発表資料です

* html版：https://sue445.gitlab.io/gitlab-meetup-tokyo-1/ [![hatebu](http://b.hatena.ne.jp/entry/image/https://sue445.gitlab.io/gitlab-meetup-tokyo-1/%23/)](http://b.hatena.ne.jp/entry/https://sue445.gitlab.io/gitlab-meetup-tokyo-1/%23/)
* markdown版：[slides.md](slides.md)

## Development
### Setup
```bash
$ bundle install
```

### Usage
```bash
$ bundle exec rake -T
rake generate  # generate slide to public/
rake serve     # serve slide
```

## Tips
* [GitLab CIでreveal-ckをビルドする時のTips - Qiita](http://qiita.com/sue445/items/2bfcdc1cfb6881e99bbd)
  * このスライド作成のTipsを紹介
